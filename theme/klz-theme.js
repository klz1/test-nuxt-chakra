//  custom-theme.js
const breakpoints = ['576px', '768px', '992px', '1200px']

breakpoints.sm = breakpoints[0]
breakpoints.md = breakpoints[1]
breakpoints.lg = breakpoints[2]
breakpoints.xl = breakpoints[3]

export default {
  breakpoints,

  fonts: {
    heading: '"kleine_titel_medium", sans-serif',
    text: '"kleine_sans_medium, sans-serif'
  },

  colors: {
    black: '#000',
    white: '#ffffff',
    red: '#c50c0e',
    primary: {
      50: '#c50c0e',
      100: '#c50c0e',
      200: '#c50c0e',
      300: '#c50c0e',
      400: '#c50c0e',
      500: '#c50c0e',
      600: '#c50c0e',
      700: '#c50c0e',
      800: '#c50c0e',
      900: '#c50c0e'
    },
    background: '#ecebe9',
    'background-darker': '#444'
  }
}
